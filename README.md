#GF.CClaufen
This is a Neos CMS site package for the Curling Club Laufen.

##Installation
Add this package to your project composer requirements.

```bash
composer require gf/cclaufen
```

Go to the root folder of this package and install Bower dependencies.

```bash
cd Packages/Sites/GF.CCLaufen
bower install
```

##Customization

###CSS
We recommend you to use customize the site's styling via SASS. Therefore you should make your changes in the `*.scss` files (eg. `Resources/Public/Styles/CCLaufen.scss`) and compile the according CSS files with the sass command.