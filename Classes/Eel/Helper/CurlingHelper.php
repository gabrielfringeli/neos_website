<?php
namespace GF\CCLaufen\Eel\Helper;

use Neos\Flow\Annotations as Flow;
use Neos\Eel\ProtectedContextAwareInterface;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Flow\Log\SystemLoggerInterface;
use \DateTime;

class CurlingHelper implements ProtectedContextAwareInterface {

    /**
     * @var SystemLoggerInterface
     * @Flow\Inject
     */
    protected $systemLogger;

    /**
    * Get all games of this tournament with specific state
    * Possible states are "running", "past" & "future"
    *
    * @param $tournament NodeInterface | null
    * @param $state string
    * @return array
    */
    public function gamesForTournament($tournament, $state='all') {

        if (is_null($tournament)) {
            return array();
        }
        
        $games = null;

        $children = $tournament->getChildNodes();
        foreach ($children as $child) {
            if ($child->getName() == 'games') {
                $games = $child->getChildNodes();
            }
        }

        switch ($state) {
            case 'all':
                return $games;
                break;
            case 'running':
                return array_filter($games, function($game) {
                    if ($game->getProperty('isFinished') == true) {
                        return false;
                    } else if ($game->getProperty('time') > new DateTime()) {
                        return false;
                    } else {
                        return true;
                    }
                });
                break;
            case 'past':
                return array_filter($games, function($game) {
                    if ($game->getProperty('isFinished') == true) {
                        return true;
                    } else {
                        return false;
                    }
                });
                break;
            case 'future':
                return array_filter($games, function($game) {
                    if ($game->getProperty('isFinished') == true) {
                        return false;
                    } else if ($game->getProperty('time') < new DateTime()) {
                        return false;
                    } else {
                        return true;
                    }
                });
                break;
        }
    }

    /**
        * Calculate the result of a game
        *
        * @param $game NodeInterface
        * @return array
        */
    public function resultsForGame(NodeInterface $game) {

        $children = $game->getChildNodes();

        foreach ($children as $child) {
            if ($child->getName() == 'ends') {

                $ends = $child->getChildNodes();

                $stonesA = 0;
                $stonesB = 0;
                $endsA = 0;
                $endsB = 0;
                $pointsA = 0;
                $pointsB = 0;
                foreach ($ends as $end) {
                    $result = intval($end->getProperty('result'));

                    if ($result < 0) {
                        $stonesB += -$result;
                        $endsB++;
                    } else if ($result > 0) {
                        $stonesA += $result;
                        $endsA++;
                    }
                }

                // calculate points
                if ($stonesA > $stonesB) {
                    $pointsA = 2;
                } else if ($stonesB > $stonesA) {
                    $pointsB = 2;
                } else {
                    $pointsA = 1;
                    $pointsB = 1;
                }

                return array(
                    "a" => array(
                        "stones" => $stonesA,
                        "ends" => $endsA,
                        "points" => $pointsA
                    ),
                    "b" => array(
                        "stones" => $stonesB,
                        "ends" => $endsB,
                        "points" => $pointsB
                    )
                );
            }
        }
    }

    /**
    * Calclulate the rankings for a tournament
    *
    * @param $tournament NodeInterface
    * @return array
    */
    public function rankingForTournament(NodeInterface $game) {
        
        $children = $game->getChildNodes();

        $teams = null;
        $games = null;

        $results = array();

        foreach ($children as $child) {
            if ($child->getName() == 'games') {
                $games = $child->getChildNodes();
            } else if ($child->getName() == 'teams') {
                $teams = $child->getChildNodes();
            }
        }

        // setup teams
        foreach ($teams as $team) {
            $results[$team->getIdentifier()] = array(
                "name" => $team->getProperty('name'),
                "results" => array(
                    "points" => 0,
                    "ends" => 0,
                    "stones" => 0,
                    "games" => 0
                )
            );
        }

        // add results, game by game
        foreach ($games as $game) {
            if ($game->getProperty('isFinished')) {

                $teamA = $game->getProperty('teamA', true);
                $teamB = $game->getProperty('teamB', true);

                $gameResults = $this->resultsForGame($game);

                $this->systemLogger->log($teamA, LOG_INFO);
                
                $results[$teamA]['results']['points'] += $gameResults['a']['points'];
                $results[$teamA]['results']['ends'] += $gameResults['a']['ends'];
                $results[$teamA]['results']['stones'] += $gameResults['a']['stones'];
                $results[$teamA]['results']['games']++;

                $results[$teamB]['results']['points'] += $gameResults['b']['points'];
                $results[$teamB]['results']['ends'] += $gameResults['b']['ends'];
                $results[$teamB]['results']['stones'] += $gameResults['b']['stones'];
                $results[$teamB]['results']['games']++;
            }
        }


        //$this->systemLogger->log(var_dump($games[0]), LOG_INFO);
        //$this->systemLogger->log(var_dump($games), LOG_INFO);

        // move key (identifier) to value part of array elements
        array_walk($results, function(&$a, $b) {
            $a['identifier'] = $b;
        });

        // remove array keys
        $results = array_values($results);

        // sort array
        usort($results, function($a, $b) {
            if ($a['results']['points'] > $b['results']['points']) {
                return -1;
            } else if ($a['results']['points'] < $b['results']['points']) {
                return 1;
            } else {
                if ($a['results']['ends'] > $b['results']['ends']) {
                    return -1;
                } else if ($a['results']['ends'] < $b['results']['ends']) {
                    return 1;
                } else {
                    if ($a['results']['stones'] > $b['results']['stones']) {
                        return -1;
                    } else if ($a['results']['stones'] < $b['results']['stones']) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        });

        return $results;
    }

    /**
        * All methods are considered safe, i.e. can be executed from within Eel
        *
        * @param string $methodName
        * @return boolean
        */
    public function allowsCallOfMethod($methodName) {
            return TRUE;
    }

}