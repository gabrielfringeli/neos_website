<?php
namespace GF\CCLaufen\DataSource;

use Neos\Neos\Service\DataSource\AbstractDataSource;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Flow\Annotations as Flow;

class ResultsForEndDataSource extends AbstractDataSource {

    /**
     * @var string
     */
    static protected $identifier = 'results-for-end';

    /**
     * @Flow\Inject
     * @var \Neos\ContentRepository\Domain\Repository\NodeDataRepository
     */
    protected $nodeDataRepository;

    /**
     * Get data
     *
     * @param NodeInterface $node The node that is currently edited (optional)
     * @param array $arguments Additional arguments (key / value)
     * @return array JSON serializable data
     */
    public function getData(NodeInterface $node = NULL, array $arguments)
    {

        $game = $node->getParent()->getParent();

        $teamA = $game->getProperty('teamA')->getProperty('name');
        $teamB = $game->getProperty('teamB')->getProperty('name');

        $results = array();
        array_push($results, array('value' => '0', 'label' => "0"));
        array_push($results, array('value' => 'X', 'label' => "X"));
        for ($i = 1; $i <= 8; $i++) {
            // positive values are points for Team A, negative values for Team B
            array_push($results, array('value' => (string)$i, 'label' => $i." für ".$teamA));
            array_push($results, array('value' => (string)(-$i), 'label' => $i." für ".$teamB));
        }

        return $results;
    }
}