<?php
namespace GF\CCLaufen\DataSource;

use Neos\Neos\Service\DataSource\AbstractDataSource;
use Neos\ContentRepository\Domain\Model\NodeInterface;

class TeamsForGameDataSource extends AbstractDataSource {

    /**
     * @var string
     */
    static protected $identifier = 'teams-for-game';

    /**
     * Get data
     *
     * @param NodeInterface $node The node that is currently edited (optional)
     * @param array $arguments Additional arguments (key / value)
     * @return array JSON serializable data
     */
    public function getData(NodeInterface $node = NULL, array $arguments)
    {

        $teamsArray = array();

        $tournamentChildNodes = $node->getParent()->getParent()->getChildNodes();
        // search for Tournament's teams child
        foreach ($tournamentChildNodes as $child) {
            if ($child->getName() == 'teams') {
                $teams = $child->getChildNodes();
                foreach ($teams as $team) {
                    array_push($teamsArray, array('value' => $team->getIdentifier(), 'label' => $team->getProperty('name')));
                }
            }
        }

        return $teamsArray;
    }
}