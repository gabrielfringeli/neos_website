var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function(done) {
    gulp.src('Resources/Private/Styles/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('Resources/Public/Styles'));
    done();
});

gulp.task('default', function() {
    gulp.watch('Resources/Private/Styles/**/*.scss', gulp.series('styles'));
});