// sticky header
$(function () {
  $(document).scroll(function () {
    var header = $("#header");
    header.toggleClass('bold', $(this).scrollTop() > 50);
  });
});

// navigation on touchscreen
(function( $, window, document, undefined ) {
    $.fn.doubleTapToGo = function( params )	{
	if( !( 'ontouchstart' in window ) &&
	    !navigator.msMaxTouchPoints &&
	    !navigator.userAgent.toLowerCase().match( /windows phone os 7/i ) ) return false;

	this.each( function() {
	    var curItem = false;

	    $( this ).on( 'click', function( e ) {
		var item = $( this );
		if( item[ 0 ] != curItem[ 0 ] ) {
		    e.preventDefault();
		    curItem = item;
		}
	    });

	    $( document ).on( 'click touchstart MSPointerDown', function( e ) {
		var resetItem = true,
		    parents	  = $( e.target ).parents();

		for( var i = 0; i < parents.length; i++ )
		    if( parents[ i ] == curItem[ 0 ] )
			resetItem = false;

		if( resetItem )
		    curItem = false;
	    });
	});
	return this;
    };
})( jQuery, window, document );
$(document).ready(function() {
    $('nav li:has(ul)').doubleTapToGo();
});

// OwlCarousel & SwipeBox

$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      }
    }
  });

  $('.owl-item').on('click', function() {
    var pswpElement = document.querySelectorAll('.pswp')[0];
    
    var items = [];
    $(this).closest('.owl-carousel').find('.neos-nodetypes-image').each(function() {
      items.push({
        src: $(this).data('large-src'),
        w: $(this).data('large-width'),
        h: $(this).data('large-height')
      });
    });
    
    var options = {
        index: $(this).index(),
        shareEl: false
    };
    
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
  });
});
